import {Component, Input} from '@angular/core';
import {PriceViewOptions} from '@surf/core';

@Component({
  selector: 'app-booking-page',
  templateUrl: './booking-page.component.html',
  styleUrls: ['./booking-page.component.css']
})
export class BookingPageComponent {

  selectedPriceViewOption: number = 1;
  loadingImage = 'assets/images/discover_3.jpg';
  @Input() hotels:any;

  setPriceView(priceView: PriceViewOptions): void {
    if(this.selectedPriceViewOption !== priceView ) {
      this.selectedPriceViewOption = priceView;
    }
  }

  onHotelDetailsExpand(hotel: any, hotelIndex?: number): void {
    hotel['showDetails'] = hotel['showDetails'] ? false : true;
    if(!hotel.expanded) {
      this.getAlternatives(hotel, hotelIndex);
    }
  }

  getAlternatives(hotel, hotelIndex?) {
  }
}
