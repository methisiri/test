import { Routes, RouterModule } from '@angular/router';
import {BookingPageComponent} from './booking-page/booking-page.component';
import {NgModule} from '@angular/core';
import {HotelDataComponent} from './hotel-data/hotel-data.component';

const routes: Routes = [
  { path: '',
    component: HotelDataComponent },
  {
    path:'ad',
    component: BookingPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookRoutingModule { }
