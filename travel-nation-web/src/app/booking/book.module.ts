import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BookRoutingModule} from './book-routing.module';
import { BookingPageComponent } from './booking-page/booking-page.component';
import { HotelDataComponent } from './hotel-data/hotel-data.component';
import { HotelsComponent } from './hotels/hotels.component';
import {SurfHotelCardBerylModule} from '@surf-widgets/surf-hotel-card-beryl';
import {SurfHotelRoomSelectionBerylModule} from '@surf-widgets/surf-hotel-room-selection-beryl';
import {SurfSortHolidayPackageBerylModule} from '@surf-widgets/surf-sort-holiday-package-beryl';
import {SurfFilterAmberModule} from '@surf-widgets/surf-filter-amber';
import {SurfInputModule} from '@surf/surf-input';
import {SurfAlertModule} from '@surf/surf-alert';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BookingService} from '../../core/service/booking.service';

@NgModule({
  imports: [
    CommonModule,
    SurfHotelCardBerylModule,
    SurfHotelRoomSelectionBerylModule,
    SurfFilterAmberModule,
    SurfSortHolidayPackageBerylModule,

    SurfInputModule,
    SurfAlertModule,
    ReactiveFormsModule,
    FormsModule,

    BookRoutingModule
  ],
  providers: [BookingService],
  declarations: [HotelsComponent,
                 BookingPageComponent,
                 HotelDataComponent],
  exports:[HotelsComponent]
})
export class BookModule { }
