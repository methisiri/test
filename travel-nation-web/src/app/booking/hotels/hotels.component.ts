import {Component} from '@angular/core';
import {BookingService} from '../../../core/service/booking.service';
import {FormControl} from '@angular/forms';
import {LEVEL} from '@surf/surf-alert';
import {HotelResults} from '../../../core/dto/HotelResults';

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})
export class HotelsComponent  {
  constructor( private bookingService: BookingService ) { }
  showPage = 1;
  hotelsData: any;
  title = 'app';
  hotelSize: any;
  checkIn = new FormControl('');
  checkOut = new FormControl('');
  adults = new FormControl('');
  rooms = new FormControl('');
  destination = new FormControl('');
  urlString: string;
  alertContent: string;
  alertLevel: LEVEL;
  cancelAlert: boolean = false;

  sendData(evnt) {
    var checkIndate = document.getElementsByName('checkIn')[0]['value'];
    var checkOutdate = document.getElementsByName('checkOut')[0]['value'];
    if(this.validate(checkIndate, checkOutdate)) {
      this.urlString = checkIndate + '_' + checkOutdate + '_' + this.adults.value + '_' + this.rooms.value + '_' + this.destination.value;
      this.bookingService.hotelList(this.urlString)
        .subscribe(
        Hotels => {
          this.showPage = 2;
          this.hotelsData = Hotels;
          // this.hotelSize = this.hotelsData.
          console.log(this.hotelsData);
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  showAlert(alertContent, alertLevel){
    this.alertContent = alertContent;
    this.alertLevel = alertLevel;
  }

  validate(checkIndate, checkOutdate): boolean {
    if(checkIndate == "" && checkOutdate == ""){
      this.showAlert("date cannot be empty", LEVEL.ERROR);
    }
    // else {
    if (this.adults.value == "") {
      this.adults.setValue('1');
    }
    if (this.rooms.value == "") {
      this.rooms.setValue('1');
    }
    if(this.destination.value == ""){
      this.destination.setValue('Colombo');
    }
    // }
    return true;
  }



}
