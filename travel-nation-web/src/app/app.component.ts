import { Component } from '@angular/core';
import {BookingService} from '../core/service/booking.service';
import {FormControl} from '@angular/forms';
import {LEVEL} from '@surf/surf-alert';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

}

