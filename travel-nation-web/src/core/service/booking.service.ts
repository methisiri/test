import {HttpClient} from '@angular/common/http';
import {EventEmitter, Injectable, Output} from '@angular/core';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class BookingService {

  private bookingUrl: String;
  protected http: HttpClient;
  bookingData: Object;
  constructor(http : HttpClient) {
    this.http = http;
  }

  hotelList(urlString: string) {
    let url = '/api' + "/ava" +'?st='+ urlString;
    console.log(url);
    return this.http.get(url).map(
      (response: Response) => {
      return response;
    });
  }
}
