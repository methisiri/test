package com.lk.service.travel.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController
{

    @GetMapping( "/" )
    public String homeMethod()
    {
        return "Hello";
    }
}
