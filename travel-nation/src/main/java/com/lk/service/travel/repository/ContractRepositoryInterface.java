package com.lk.service.travel.repository;

import com.lk.service.travel.models.Contract;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @package: com.lk.service.travel.repository
 * @author: methsiri
 * @date: 10/28/2019
 * @time: 11:53 AM
 */
@Repository
public interface ContractRepositoryInterface extends JpaRepository<Contract,Integer>
{
    List<Contract> findAllByFromDateLessThanEqualAndToDateGreaterThanEqual(Date fromDate, Date toDate );
}
