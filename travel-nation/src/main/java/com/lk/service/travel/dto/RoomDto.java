package com.lk.service.travel.dto;

import org.springframework.stereotype.Component;

/**
 * @package: com.lk.service.travel.dto
 * @author: methsiri
 * @date: 10/30/2019
 * @time: 2:06 PM
 */

public class RoomDto
{
    private int hotelId;
    private double price;
    private String roomType;
    private int adultCount;

    public RoomDto( double price, String roomType, int adultCount )
    {
        this.price = price;
        this.roomType = roomType;
        this.adultCount = adultCount;
    }
    public double getPrice()
    {
        return price;
    }

    public String getRoomType()
    {
        return roomType;
    }

    public int getAdultCount()
    {
        return adultCount;
    }

    public void setAdultCount( int adultCount )
    {
        this.adultCount = adultCount;
    }

    public void setPrice( double price )
    {
        this.price = price;
    }

    public void setRoomType( String roomType )
    {
        this.roomType = roomType;
    }

    public int getHotelId()
    {
        return hotelId;
    }

    public void setHotelId( int hotelId )
    {
        this.hotelId = hotelId;
    }

}
