package com.lk.service.travel.repository;

import com.lk.service.travel.models.Contract;
import com.lk.service.travel.models.Markup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @package: com.lk.service.travel.repository
 * @author: methsiri
 * @date: 10/29/2019
 * @time: 7:04 PM
 */
@Repository
public interface MarkupRepositoryInterface extends JpaRepository<Markup,Integer>
{
    Markup findAllByContract(Contract contract);
}
