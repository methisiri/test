package com.lk.service.travel.dto;

import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @package: com.lk.service.travel.dto
 * @author: methsiri
 * @date: 10/23/2019
 * @time: 6:10 PM
 */
@Component
public class HotelDto
{
    private int hotelId;
    private String name;
    private String address;
    private int tpnumber;
    private String status;
    private List<RoomDto> room;

    public int getHotelId()
    {
        return hotelId;
    }

    public String getName()
    {
        return name;
    }

    public String getAddress()
    {
        return address;
    }

    public int getTpnumber()
    {
        return tpnumber;
    }

    public String getStatus()
    {
        return status;
    }

    public List<RoomDto> getRoom()
    {
        return room;
    }

    public void setRoom( List<RoomDto> room )
    {
        this.room = room;
    }

    public void setHotelId( int hotelId )
    {
        this.hotelId = hotelId;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    public void setAddress( String address )
    {
        this.address = address;
    }

    public void setTpnumber( int tpnumber )
    {
        this.tpnumber = tpnumber;
    }

    public void setStatus( String status )
    {
        this.status = status;
    }
}
