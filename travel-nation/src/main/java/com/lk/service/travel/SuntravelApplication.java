package com.lk.service.travel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuntravelApplication
{

    public static void main( String[] args )
    {
        SpringApplication.run( SuntravelApplication.class, args );
    }

}
