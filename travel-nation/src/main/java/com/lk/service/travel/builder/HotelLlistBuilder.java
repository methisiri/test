package com.lk.service.travel.builder;

import com.lk.service.travel.dto.HotelDto;

import java.util.ArrayList;
import java.util.List;

/**
 * @package: com.lk.service.travel.builder
 * @author: methsiri
 * @date: 10/23/2019
 * @time: 6:08 PM
 */
public class HotelLlistBuilder
{
    public List<HotelDto> hotelLists( List<HotelDto> dbHotels )
    {
        List<HotelDto> hotelDtos = new ArrayList<>();
        dbHotels.forEach( dbHotel ->
        {
            HotelDto hotelDto = dbHotel;
//            hotelDto.setHotelId( e1134Hotel.getHotelId() );
//            hotelDto.setName( e1134Hotel.getName() );
//            hotelDto.setAddress( e1134Hotel.getAddress() );
            hotelDtos.add( hotelDto );
        } );
        return hotelDtos;
    }
}
