package com.lk.service.travel.repository;

import com.lk.service.travel.models.Contract;
import com.lk.service.travel.models.RoomContract;

import java.util.List;

/**
 * @package: com.lk.service.travel.repository
 * @author: methsiri
 * @date: 10/28/2019
 * @time: 3:31 PM
 */
@FunctionalInterface
public interface SuntravelRepositoryInterface
{
    List<RoomContract> hotelDetails(List<Contract> contracts, int adults, int rooms, String destination );
}
