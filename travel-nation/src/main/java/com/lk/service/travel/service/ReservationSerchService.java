package com.lk.service.travel.service;

import com.lk.service.travel.dto.HotelDto;
import com.lk.service.travel.dto.RoomDto;
import com.lk.service.travel.models.Contract;
import com.lk.service.travel.models.Markup;
import com.lk.service.travel.repository.ContractRepositoryInterface;
import com.lk.service.travel.repository.MarkupRepositoryInterface;

import com.lk.service.travel.repository.SuntravelRepositoryInterface;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @package: com.cgen.service.suntravel.service
 * @author: methsiri
 * @date: 10/23/2019
 * @time: 5:58 PM
 */

@Service
public class ReservationSerchService
{
    @Autowired
    private ContractRepositoryInterface contractRepository;

    @Autowired
    private SuntravelRepositoryInterface suntravelRepository;

    @Autowired
    private MarkupRepositoryInterface markupRepositoryInterface;

    /**
     * @param st
     * @return search results HotelDto
     * @throws ParseException
     */
    public Map<Integer,HotelDto> avilableReservations( String st ) throws ParseException
    {

        //return value
        Map<Integer,HotelDto> hotelDtos = new HashMap<>();
        List<RoomDto> roomDtos = new ArrayList<>();

        String[] criteriaAarray = st.split( "_" );
        Date fdate = new SimpleDateFormat( "MM/dd/yyyy" ).parse( criteriaAarray[0] );
        Date tdate = new SimpleDateFormat( "MM/dd/yyyy" ).parse( criteriaAarray[1] );
        int adults = Integer.parseInt( criteriaAarray[2] );
        int rooms = Integer.parseInt( criteriaAarray[3] );
        String destination = criteriaAarray[4];

        //#of Nights
        long diffInMillies = Math.abs( tdate.getTime() - fdate.getTime() );
        int numOfNights = ( int ) TimeUnit.DAYS.convert( diffInMillies, TimeUnit.MILLISECONDS );

        //Get available contracts
        List<Contract> contracts = contractRepository.findAllByFromDateLessThanEqualAndToDateGreaterThanEqual( fdate, tdate );

        //set pricing and return hotels
        suntravelRepository.hotelDetails( contracts, adults, rooms, destination ).stream().forEach( roomContract ->
        {
            Hibernate.initialize( roomContract.getRoomType() );
            Hibernate.initialize( roomContract.getRoomType().getHotel() );

            RoomDto room = new RoomDto( getMarkup( roomContract.getContract() ).getPrice() * roomContract.getPrice() * adults * numOfNights +
                                                roomContract.getPrice() * adults * numOfNights, roomContract.getRoomType().getType(), roomContract.getRoomType().getAdultCount() );
            room.setHotelId( roomContract.getRoomType().getHotel().getHotelId() );
            roomDtos.add( room );

            HotelDto hotel = new HotelDto();
            hotel.setName( roomContract.getRoomType().getHotel().getName() );
            hotel.setAddress( roomContract.getRoomType().getHotel().getAddress() );
            hotel.setTpnumber( roomContract.getRoomType().getHotel().getTpnumber() );
            hotel.setHotelId( roomContract.getRoomType().getHotel().getHotelId() );

            hotelDtos.put( hotel.getHotelId(), hotel );
        } );

        return createaHotelList( hotelDtos, roomDtos );
    }

    /**
     * get a hotel map which include rooms
     *
     * @param hotelDtos
     * @param roomDtos
     * @return Map<Integer, HotelDto>
     */
    private Map<Integer,HotelDto> createaHotelList( Map<Integer,HotelDto> hotelDtos, List<RoomDto> roomDtos )
    {
        hotelDtos.forEach( ( integer, hotelDto ) ->
        {

            List<RoomDto> rooms = new ArrayList<>();
            roomDtos.forEach( roomDto ->
            {
                if( roomDto.getHotelId() == integer )
                {
                    rooms.add( roomDto );
                }
            } );

            hotelDto.setRoom( rooms );
        } );
        return hotelDtos;
    }

    //get markups for contracts
    private Markup getMarkup(Contract contracts )
    {
        return markupRepositoryInterface.findAllByContract( contracts );
    }

}
