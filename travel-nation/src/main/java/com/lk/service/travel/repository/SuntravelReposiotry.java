package com.lk.service.travel.repository;

import com.lk.service.travel.models.Contract;
import com.lk.service.travel.models.RoomContract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * @package: com.lk.service.travel.repository
 * @author: methsiri
 * @date: 10/28/2019
 * @time: 3:32 PM
 */

@Repository
public class SuntravelReposiotry implements SuntravelRepositoryInterface
{

    @Autowired
    private EntityManager entityManager;

    /**
     *
     * @param contracts
     * @param adults
     * @param rooms
     * @param destination
     * @return RoomContract
     */
    @Override
    public List<RoomContract> hotelDetails(List<Contract> contracts, int adults, int rooms, String destination )
    {
        List<RoomContract> roomContracts = new ArrayList<>();

        StringBuilder builder = new StringBuilder( " SELECT * "
                                                           + " FROM ROOM_CONTRACT RC, ROOM_TYPE RT, HOTEL H "
                                                           + " WHERE RC.ROOM_TYPE_ID = RT.ROOM_ID "
                                                           + " AND RT.HOTEL_ID = H.HOTEL_ID "
        );

        if( adults != 1 )
        {
            builder.append( " AND RT.ADULT_COUNT = " + adults );
        }
        if( rooms != 1 )
        {
            builder.append( " AND RC.AVAILABLE_ROOM_COUNT >= " + rooms );
        }
        if( !destination.equals( "Colombo" ) )
        {
            builder.append( " AND H.ADDRESS LIKE '%" + destination + "%'" );
        }
        else
        {
            builder.append( " AND H.ADDRESS LIKE '%Colombo%'" );
        }

        Query nativeQuery = entityManager.createNativeQuery( builder.toString(), RoomContract.class );
        roomContracts = nativeQuery.getResultList();
        return roomContracts;
    }

}
