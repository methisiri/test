package com.lk.service.travel.repository;

import com.lk.service.travel.models.Contract;
import com.lk.service.travel.models.RoomContract;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @package: com.lk.service.travel.repository
 * @author: methsiri
 * @date: 10/23/2019
 * @time: 7:46 PM
 */

@Repository
public interface RoomContractRepositoryInterface extends JpaRepository<RoomContract,Integer>
{
    RoomContract findAllByContract(Contract contract);
}
