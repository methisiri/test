package com.lk.service.travel.repository;

import com.lk.service.travel.models.Hotel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @package: com.lk.service.travel.repository
 * @author: methsiri
 * @date: 10/28/2019
 * @time: 11:55 AM
 */
@Repository
public interface HotelRepositoryInterface extends JpaRepository<Hotel,Integer>
{
}
