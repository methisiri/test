package com.lk.service.travel.controller;

import com.lk.service.travel.dto.HotelDto;
import com.lk.service.travel.service.ReservationSerchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @package: com.lk.service.travel
 * @author: methsiri
 * @date: 10/23/2019
 * @time: 5:42 PM
 */
@CrossOrigin( origins = "http://localhost:4201", maxAge = 3600 )
@RestController
public class ReservationSerchController
{
    @Autowired
    private ReservationSerchService serchService;

    @Autowired
    private static Logger logger;

    @GetMapping( "ava" )
    public List<HotelDto> avilableReservations( String st )
    {
        Map<Integer,HotelDto> contracts = null;
        List<HotelDto> conts = null;
        try
        {
            contracts = serchService.avilableReservations( st );
            conts = new ArrayList<>(contracts.values());
        }
        catch( Exception e )
        {
            logger.log( Level.SEVERE, e.toString() );
        }
        return conts;
    }
}
